# write down a python code that exemplifies SQL injection pattern for a training purpose

import sqlite3

def unsafe_login(username, password):
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()
    
    # Unsafe SQL query vulnerable to SQL injection
    
    query = f"SELECT * FROM users WHERE username = '{username}' AND password = '{password}'"
    cursor.execute(query)
    
    user = cursor.fetchone()
    
    conn.close()
    
    if user:
        return "Login successful"
    else:
        return "Login failed"